<?php
include("conexao.php");
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Maruti Admin</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="css/colorpicker.css" />
<link rel="stylesheet" href="css/datepicker.css" />
<link rel="stylesheet" href="css/uniform.css" />
<link rel="stylesheet" href="css/select2.css" />
<link rel="stylesheet" href="css/maruti-style.css" />
<link rel="stylesheet" href="css/maruti-media.css" class="skin-color" />
</head>
<body>

<!--Header-part-->
<div id="header">
  <h1><a href="dashboard.php">Maruti Admin</a></h1>
</div>
<!--close-Header-part--> 

<!--top-Header-messaages-->
<div class="btn-group rightzero"> <a class="top_message tip-left" title="Manage Files"><i class="icon-file"></i></a> <a class="top_message tip-bottom" title="Manage Users"><i class="icon-user"></i></a> <a class="top_message tip-bottom" title="Manage Comments"><i class="icon-comment"></i><span class="label label-important">5</span></a> <a class="top_message tip-bottom" title="Manage Orders"><i class="icon-shopping-cart"></i></a> </div>
<!--close-top-Header-messaages--> 

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
<li class="" ><a title="" href="#"><i class="icon icon-user"></i><?php if (isset($_SESSION['email'])){echo $nome=" ", $_SESSION['nome'];}else echo"Profile";?></a></li>
     <li class=""><a title="" href="authentication-register.php"> <span class="text">Registre-se</span></a></li>
    <li class=""><a title="" href="logout.php"><i class="icon icon-share-alt"></i> <span class="text"><?php if (isset($_SESSION['email'])){echo "logout";}else echo"login";?></span></a></li>
  </ul>
</div>
  <div id="search">
    <input type="text" placeholder="Search here..."/>
    <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
  </div>
<!--close-top-Header-menu--> 
<!--left-menu-stats-sidebar-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a><ul>
    <li class="active"><a href="index_para_mudar.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li><a href="CriarPergunta.php"><i class="icon icon-tint"></i> <span>Crie sua pergunta</span></a></li>
    
  </ul>
</div>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index_para_mudar.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <div class="container-fluid">
   	<div class="quick-actions_homepage">
    <ul class="quick-actions">
          <li> <a href="Programacao.php"> <i class="icon-dashboard"></i>Programação</a> </li>
          <li> <a href="Matematica.php"> <i class="icon-shopping-bag"></i>Matemática</a> </li>
          <li> <a href="Redes.php"> <i class="icon-web"></i>Redes</a> </li>
          <li> <a href="Analise.php"> <i class="icon-people"></i>Análise e desenvolvimento de projetos</a> </li>
          <li> <a href="ProgramacaoJogos.php"> <i class="icon-calendar"></i>Progamação p/ jogos</a> </li>
        </ul>
   </div>
   
    
      <div class="rowfluid">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-refresh"></i> </span>
            <h5>News updates</h5>
          </div>
          <div class="widget-content nopadding updates">
            <div class="new-update clearfix"><i class="icon-ok-sign"></i>
              <div class="update-done"><a title="" href="#"><strong>Adicionado todas as páginas das matérias cadastradas</strong></a> <span>Páginas de Matemática, Programação, Redes, Análises e Programação para jogos adicionadas</span> </div>
              <span class="update-date"><span class="update-day">20</span>jan</span></div>
              
            <div class="new-update clearfix"> <i class="icon-ok-sign"></i> <span class="update-notice"> <a title="" href="#"><strong>Página de login adcionada</strong></a> <span>pagina de login adicionada ao site</span> </span> <span class="update-date"><span class="update-day">11</span>jan</span> </div>
              
            <div class="new-update clearfix"> <i class="icon-ok-sign"></i> <span class="update-alert"> <a title="" href="#"><strong>Página de registro sendo desolvida</strong></a> <span>Logo sera adiciona uma página para registro de contas ao site</span> </span> <span class="update-date"><span class="update-day">07</span>Jan</span> </div>
              
            <div class="new-update clearfix"> <i class="icon-ok-sign"></i> <span class="update-done"> <a title="" href="#"><strong>Desenvolvimento do banco de dados cocluido</strong></a> <span>Logo sera implementado o banco de dados ao site</span> </span> <span class="update-date"><span class="update-day">05</span>jan</span> </div>
              
            <div class="new-update clearfix"> <i class="icon-question-sign"></i> <span class="update-notice"> <a title="" href="#"><strong>Nos de sua sugestão para a melhor adapatação do site</strong></a> <span>Contamos com sua avalição para melhor desenvolver o site</span> </span> <span class="update-date"><span class="update-day">01</span>jan</span> </div>
             
             <div class="new-update clearfix"> <i class="icon-question-sign"></i> <span class="update-notice"> <a title="" href="#"><strong>Sistema de login/cadastro e envio de perguntas funcional</strong></a> <span>Parte de sessions de uma conta ainda em desenvolvimento</span> </span> <span class="update-date"><span class="update-day">01</span>jan</span> </div>
              
          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="row-fluid">
      <div class="row-fluid">
        <div class="accordion" id="collapse-group">
          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5>Termos de uso 1</h5>
                </a> </div>
            </div>
            <div class="collapse in accordion-body" id="collapseGOne">
              <div class="widget-content"> Este é um site com objetivo educacional portanto todo e qualquer post que não seja deste assunto sera excluído e a conta será suspensa podendo ser banida do site. </div>
            </div>
          </div>
          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseGTwo" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5>Termos de uso 2</h5>
                </a> </div>
            </div>
            <div class="collapse accordion-body" id="collapseGTwo">
              <div class="widget-content">Para efetuar o registro é nescessário que voce tenha um e-mail acadêmico, pois o site é voltado apenas para instituições de ensino</div>
            </div>
          </div>
          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseGThree" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5>Termos de uso 3</h5>
                </a> </div>
            </div>
            <div class="collapse accordion-body" id="collapseGThree">
              <div class="widget-content">Ao usar o IFOverFlow, voce concorda com todos os termos de uso do nosso site.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<div class="row-fluid">
  <div id="footer" class="span12"> 2012 &copy; Marutii Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>
</div>
<script src="js/excanvas.min.js"></script> 
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.flot.min.js"></script> 
<script src="js/jquery.flot.resize.min.js"></script> 
<script src="js/jquery.peity.min.js"></script> 
<script src="js/fullcalendar.min.js"></script> 
<script src="js/maruti.js"></script> 
<script src="js/maruti.dashboard.js"></script> 
<script src="js/maruti.chat.js"></script> 
 

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>
