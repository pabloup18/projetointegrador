<?php
include("conexao.php");
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Maruti Admin</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="css/colorpicker.css" />
<link rel="stylesheet" href="css/datepicker.css" />
<link rel="stylesheet" href="css/uniform.css" />
<link rel="stylesheet" href="css/select2.css" />
<link rel="stylesheet" href="css/maruti-style.css" />
<link rel="stylesheet" href="css/maruti-media.css" class="skin-color" />
</head>
<body>

<!--Header-part-->
<div id="header">
  <h1><a href="dashboard.php">Maruti Admin</a></h1>
</div>
<!--close-Header-part--> 

<!--top-Header-messaages-->
<div class="btn-group rightzero"> <a class="top_message tip-left" title="Manage Files"><i class="icon-file"></i></a> <a class="top_message tip-bottom" title="Manage Users"><i class="icon-user"></i></a> <a class="top_message tip-bottom" title="Manage Comments"><i class="icon-comment"></i><span class="label label-important">5</span></a> <a class="top_message tip-bottom" title="Manage Orders"><i class="icon-shopping-cart"></i></a> </div>
<!--close-top-Header-messaages--> 

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
<li class="" ><a title="" href="#"><i class="icon icon-user"></i><?php if (isset($_SESSION['email'])){echo $nome=" ", $_SESSION['nome'];}else echo"Profile";?></a></li>
     <li class=""><a title="" href="authentication-register.php"> <span class="text">Registre-se</span></a></li>
    <li class=""><a title="" href="logout.php"><i class="icon icon-share-alt"></i> <span class="text"><?php if (isset($_SESSION['email'])){echo "logout";}else echo"login";?></span></a></li>
  </ul>
</div>
  <div id="search">
    <input type="text" placeholder="Search here..."/>
    <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
  </div>
<!--close-top-Header-menu--> 
<!--left-menu-stats-sidebar-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a><ul>
    <li class="active"><a href="index_para_mudar.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li><a href="CriarPergunta.php"><i class="icon icon-tint"></i> <span>Crie sua pergunta</span></a></li>
    
  </ul>
</div>
<!--close-left-menu-stats-sidebar-->

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> </div>
    <h1 >Faça sua Pergunta </h1>
  </div>
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="row-fluid">
        <div class="widget-box">
          <div class="widget-content nopadding">
            <form method="post" class="form-horizontal" action="cadastro_pergunta.php">
              <div class="control-group">
                <label class="control-label"><b>Título</b></label>
                <div class="controls">
                  <input name="titulo" type="text" class="span11" placeholder="Título da Pergunta" />
                </div>
                </div>
              
                <div class="control-group">
                    <label class="control-label"><b>Unidade Curricular</b> </label>
                <div class="controls">
                  <select name="unidade_curricular">
                    <option value="1"> Análise e desenvolvimento de projetos</option>
                    <option value="2">Matemática</option>
                    <option value="3">Programação </option>
                    <option value="4">Programação para Jogos</option>
                    <option value="5">Redes</option>
                  </select>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label"><b>Pergunta</b></label>
                <div class="controls">
                  <textarea name="pergunta" class="span11" style="height: 200px;"></textarea>
                </div>
              </div>
                <div class="control-group ">
                     <label class="control-label"><b>Proibições:</b></label>
                </div>
              <div class="form-actions">
                <button type="submit" class="btn btn-success"><b>Salvar</b> </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      </div>
    </div><hr>
  </div>
<div class="row-fluid">
  <div id="footer" class="span12"> 2012 &copy; Marutii Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>
</div>
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-colorpicker.js"></script> 
<script src="js/bootstrap-datepicker.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/maruti.js"></script> 
<script src="js/maruti.form_common.js"></script>
</body>
</html>
