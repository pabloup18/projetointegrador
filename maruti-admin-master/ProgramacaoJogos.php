<?php
include("conexao.php");
session_start();
$sel = "select * from pergunta where Unidade_Curricular = '4'";
$con = $conexao->query($sel) or die ($conexao->error);

?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Maruti Admin</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="css/colorpicker.css" />
<link rel="stylesheet" href="css/datepicker.css" />
<link rel="stylesheet" href="css/uniform.css" />
<link rel="stylesheet" href="css/select2.css" />
<link rel="stylesheet" href="css/maruti-style.css" />
<link rel="stylesheet" href="css/maruti-media.css" class="skin-color" />
</head>
<body>

<!--Header-part-->
<div id="header">
  <h1><a href="dashboard.php">Maruti Admin</a></h1>
</div>
<!--close-Header-part--> 

<!--top-Header-messaages-->
<div class="btn-group rightzero"> <a class="top_message tip-left" title="Manage Files"><i class="icon-file"></i></a> <a class="top_message tip-bottom" title="Manage Users"><i class="icon-user"></i></a> <a class="top_message tip-bottom" title="Manage Comments"><i class="icon-comment"></i><span class="label label-important">5</span></a> <a class="top_message tip-bottom" title="Manage Orders"><i class="icon-shopping-cart"></i></a> </div>
<!--close-top-Header-messaages--> 

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
<li class="" ><a title="" href="#"><i class="icon icon-user"></i><?php if (isset($_SESSION['email'])){echo $nome=" ", $_SESSION['nome'];}else echo"Profile";?></a></li>
     <li class=""><a title="" href="authentication-register.php"> <span class="text">Registre-se</span></a></li>
    <li class=""><a title="" href="logout.php"><i class="icon icon-share-alt"></i> <span class="text"><?php if (isset($_SESSION['email'])){echo "logout";}else echo"login";?></span></a></li>
  </ul>
</div>
  <div id="search">
    <input type="text" placeholder="Search here..."/>
    <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
  </div>
<!--close-top-Header-menu--> 
<!--left-menu-stats-sidebar-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a><ul>
    <li class="active"><a href="index_para_mudar.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li><a href="CriarPergunta.php"><i class="icon icon-tint"></i> <span>Crie sua pergunta</span></a></li>
    
  </ul>
</div>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index_para_mudar.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <div class="container-fluid">
   	<div class="quick-actions_homepage">
    <ul class="quick-actions">
          <li> <a href="Programacao.php"> <i class="icon-dashboard"></i>Programação</a> </li>
          <li> <a href="Matematica.php"> <i class="icon-shopping-bag"></i>Matematica</a> </li>
          <li> <a href="Redes.php"> <i class="icon-web"></i>Redes</a> </li>
          <li> <a href="Analise.php"> <i class="icon-people"></i>Analise e desenvolvimento de projetos</a> </li>
          <li> <a href="ProgramacaoJogos.php"> <i class="icon-calendar"></i>Progamação p/ jogos</a> </li>
        </ul>
   </div>
     <div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-file"></i>
								</span>
								<h5>Recent Posts</h5>
							</div>
							<div class="widget-content nopadding">
								<ul class="recent-posts">
                     <?php while($dado = $con->fetch_array()){?>
                     
                     <?php
                        
                      ?>
                      
                      <li>
                        <div class="user-thumb"> <img width="40" height="40" alt="User" src="img/demo/av1.jpg"> </div>
                        <div class="article-post"> 
                        	<div class="fr"><a href="#" class="btn btn-primary btn-mini">Edit</a> <a href="#" class="btn btn-success btn-mini">Publish</a> <a href="#" class="btn btn-danger btn-mini">Delete</a></div>
                          <span class="user-info"> By:<?php 
                        $idu = $dado["Id_Usuario"];
                        $idu = intval($idu);

                        $cma = "select Nome from usuario where Id = $idu "; 

                        $id = $dado["Id"];
                        $cm = $conexao->query($cma) or die ($conexao->error);
                        $nome= $cm->fetch_array();
                        echo "  ", $nome['Nome'];?> </span>
                        <p><a href="http://localhost/projetointegrador/maruti-admin-master/tables.php?id= <?php echo $id;?>">
                        <?php echo $dado["Titulo"]?></a> </p>
                          
                          </div>
                      </li>
                      <?php }?>
                     <li><button class="btn btn-warning btn-mini">View All</button></li>
                    </ul> 
                            </div>										
                        </div>										
                    </div>										
    </div>										
    </div>
    </div>
<script src="js/excanvas.min.js"></script> 
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.flot.min.js"></script> 
<script src="js/jquery.flot.resize.min.js"></script> 
<script src="js/jquery.peity.min.js"></script> 
<script src="js/fullcalendar.min.js"></script> 
<script src="js/maruti.js"></script> 
<script src="js/maruti.dashboard.js"></script> 
<script src="js/maruti.chat.js"></script> 
 

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>
